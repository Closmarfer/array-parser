setup: docker-build up

# Docker tasks
docker-build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down

restart: down up

bash:
	docker-compose exec app bash

logs:
	docker-compose logs -f app

# Go format tasks
linter: format
	go run github.com/golangci/golangci-lint/cmd/golangci-lint run --out-format checkstyle --deadline=3m

format: ## Fixes format errors by applying the canonical Go style
	go run mvdan.cc/gofumpt -d -s -w cmd pkg

go-mocks:
	@echo "generating mock files ..."
	find $(CURDIR) -name "mock_*.go" -delete
	go generate -v -run="mockgen" ./...

tests:
	@echo "running unit tests ..."
	go test ./cmd/... ./pkg/... -cover
	@echo "... done"
# Array Parser

## Setup

This project is running in Docker using `docker-compose`.
Execute `make setup` to build the required Docker image and execute the app.
The API is accessible via [localhost:3000](http://localhost:3000)

## Test the endpoints

Import to Postman the collection of request placed within the [postman](postman) folder.

## Testing

Access to the container bash using `make bash` and execute `make tests` to run
the complete suite of tests.

## The Project

I have approached the parser as an application agnostic to the elements that compose the array.

On the one hand we have the parser itself [pkg/parser/parser.go](pkg/parser/parser.go) which should
flattening the array and validating it.

On the other hand we have the tokenizer, which separates the array into its minimal components
(openings, closings, separators and content elements).

The tokenizer comes defined by an interface in the `parser.go` file, with methods to determine
whether an element is an opening, closing, separator or content element. In this way we can
support different types of arrays.

The proposed array uses as openings and closings the square brackets `[ ]`, as separator the commas `,`
and the content elements are all considered as `string`.  
See [pkg/parser/tokenizers/tokenizer.go](pkg/parser/tokenizers/tokenizer.go).

However we could implement, for example, another `tokenizer` that would 
use as opening and closing parenthesis `( )` and as separator semicolon `;`.

Regarding the storage to store the history of parsed arrays, I have chosen Redis, 
using a List to store the parses in order of addition, inserting the most recent one at the first position
using the `LPUSH` command.

## TODO

* Add `wire` [https://github.com/google/wire](https://github.com/google/wire) as a Service Provider
* Separate the http handlers ([pkg/http/routes.go](pkg/http/routes.go)) in different files and test each of them
* Define parameters by environment (eg. the Redis host and the exposed server host and port are defined directly 
  in the sourcecode)
* Add logger to log errors in the backend
package main

import (
	"array-parser/pkg/platform/redis"
	"array-parser/pkg/serviceprovider"

	"github.com/gin-gonic/gin"

	http2 "array-parser/pkg/http"
)

func main() {
	parser := serviceprovider.ProvideParser(redis.Config{Address: "redis:6379"})
	routes := http2.NewRoutes(*parser)

	s := http2.NewHTTPServer(http2.Config{Address: ":3000"}, gin.Default(), routes.ProvideRoutes())

	if err := s.Run(); err != nil {
		panic(err)
	}
}

module array-parser

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis/v8 v8.6.0
	github.com/golang/mock v1.3.1
	github.com/golangci/golangci-lint v1.37.1
	github.com/stretchr/testify v1.7.0
	mvdan.cc/gofumpt v0.1.0
)

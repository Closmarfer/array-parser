package http

import (
	"array-parser/pkg/parser"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type Routes struct {
	parser parser.Parser
}

func NewRoutes(arrayParser parser.Parser) *Routes {
	return &Routes{parser: arrayParser}
}

func (r Routes) ProvideRoutes() []Route {
	routes := make([]Route, 3)

	routes[0] = Route{
		handler: r.welcome,
		path:    "/",
		method:  "GET",
	}

	routes[1] = Route{
		handler: r.arrayParseHandler,
		path:    "/array",
		method:  "POST",
	}

	routes[2] = Route{
		handler: r.arrayParseListHandler,
		path:    "/array",
		method:  "GET",
	}

	return routes
}

func (r Routes) welcome(c *gin.Context) {
	c.String(http.StatusOK, "Welcome to the Array Parser")
}

func (r Routes) arrayParseHandler(c *gin.Context) {
	input := c.PostForm("input")

	if input == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "input param is required",
		})

		return
	}

	result, err := r.parser.Parse(input, time.Now())
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"parsed": result.Output,
		"depth":  result.Depth,
	})
}

func (r Routes) arrayParseListHandler(c *gin.Context) {
	result, err := r.parser.GetLatest()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Internal server error",
		})

		return
	}

	c.JSON(http.StatusOK, result)
}

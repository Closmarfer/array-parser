package http

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Route struct {
	handler gin.HandlerFunc
	path    string
	method  string
}

type Config struct {
	Address string
}

type Server struct {
	config     Config
	HTTPEngine *gin.Engine
	routes     []Route
}

func NewHTTPServer(config Config, ginEngine *gin.Engine, routes []Route) *Server {
	s := &Server{config: config, HTTPEngine: ginEngine, routes: routes}

	s.defineRoutes()

	return s
}

func (s Server) Run() error {
	srv := &http.Server{
		Addr:    ":3000",
		Handler: s.HTTPEngine,
	}

	return srv.ListenAndServe()
}

func (s Server) defineRoutes() {
	for _, route := range s.routes {
		if route.method == "POST" {
			s.HTTPEngine.POST(route.path, route.handler)

			continue
		}

		s.HTTPEngine.GET(route.path, route.handler)
	}
}

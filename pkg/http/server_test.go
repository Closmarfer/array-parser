package http

import (
	"array-parser/pkg/parser"
	"array-parser/pkg/platform/redis"
	"array-parser/pkg/serviceprovider"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/require"
)

func TestServer(t *testing.T) {
	req := require.New(t)

	t.Run("Test base path", func(t *testing.T) {
		srv, w := provideServer()

		r, err := http.NewRequest("GET", "/", nil)
		req.NoError(err)

		srv.HTTPEngine.ServeHTTP(w, r)

		req.Equal(http.StatusOK, w.Code)
		req.Equal("Welcome to the Array Parser", w.Body.String())
	})

	t.Run("Array parse path should return error of required param", func(t *testing.T) {
		srv, w := provideServer()

		body := strings.NewReader("")
		r, err := http.NewRequest("POST", "/array", body)
		req.NoError(err)

		r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

		srv.HTTPEngine.ServeHTTP(w, r)

		req.Equal(http.StatusBadRequest, w.Code)
		req.Equal(`{"error":"input param is required"}`, w.Body.String())
	})

	t.Run("Array parse path should return error of invalid array format", func(t *testing.T) {
		srv, w := provideServer()

		body := strings.NewReader(`input=[23,34]]`)
		r, err := http.NewRequest("POST", "/array", body)
		req.NoError(err)

		r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

		srv.HTTPEngine.ServeHTTP(w, r)

		req.Equal(http.StatusBadRequest, w.Code)
		req.Equal(`{"error":"invalid format: invalid array delimiters count: -1"}`, w.Body.String())
	})

	t.Run("Array should be parsed", func(t *testing.T) {
		client := getClient()
		err := redis.FlushAll(client)
		srv, w := provideServer()

		body := strings.NewReader(`input=[23,[34]]`)
		r, err := http.NewRequest("POST", "/array", body)
		req.NoError(err)

		r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

		srv.HTTPEngine.ServeHTTP(w, r)

		req.Equal(http.StatusOK, w.Code)
		req.Equal(`{"depth":1,"parsed":"[23,34]"}`, w.Body.String())
	})

	t.Run("List of processed arrays should be retrieved", func(t *testing.T) {
		client := getClient()
		err := redis.FlushAll(client)
		req.NoError(err)

		strg := serviceprovider.ProvideArrayStorage(client)

		created, err := time.Parse("2006-01-02", "2020-02-21")
		req.NoError(err)

		err = strg.Save(parser.Result{
			Input:  "[test,[output]]",
			Output: "[test,output]",
			Depth:  1,
			Date:   created,
		})

		req.NoError(err)

		srv, w := provideServer()

		r, err := http.NewRequest("GET", "/array", nil)

		srv.HTTPEngine.ServeHTTP(w, r)

		req.Equal(http.StatusOK, w.Code)
		req.Equal(`[{"input":"[test,[output]]","output":"[test,output]","depth":1,"date":"2020-02-21T00:00:00Z"}]`, w.Body.String())
	})
}

func provideServer() (*Server, *httptest.ResponseRecorder) {
	p := serviceprovider.ProvideParser(getRedisConfig())
	routes := NewRoutes(*p)

	srv := NewHTTPServer(Config{Address: ":8899"}, gin.Default(), routes.ProvideRoutes())

	w := httptest.NewRecorder()

	return srv, w
}

func getClient() redis.Client {
	return serviceprovider.ProvideRedisClient(getRedisConfig())
}

func getRedisConfig() redis.Config {
	return redis.Config{Address: "redis:6379"}
}

package parser

func (p Parser) GetLatest() ([]Result, error) {
	return p.storage.GetLatest()
}

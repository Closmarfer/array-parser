// Code generated by MockGen. DO NOT EDIT.
// Source: parser.go

// Package parser is a generated GoMock package.
package parser

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockTokenizer is a mock of Tokenizer interface
type MockTokenizer struct {
	ctrl     *gomock.Controller
	recorder *MockTokenizerMockRecorder
}

// MockTokenizerMockRecorder is the mock recorder for MockTokenizer
type MockTokenizerMockRecorder struct {
	mock *MockTokenizer
}

// NewMockTokenizer creates a new mock instance
func NewMockTokenizer(ctrl *gomock.Controller) *MockTokenizer {
	mock := &MockTokenizer{ctrl: ctrl}
	mock.recorder = &MockTokenizerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockTokenizer) EXPECT() *MockTokenizerMockRecorder {
	return m.recorder
}

// ParseTokens mocks base method
func (m *MockTokenizer) ParseTokens(input string) ([]string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ParseTokens", input)
	ret0, _ := ret[0].([]string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ParseTokens indicates an expected call of ParseTokens
func (mr *MockTokenizerMockRecorder) ParseTokens(input interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ParseTokens", reflect.TypeOf((*MockTokenizer)(nil).ParseTokens), input)
}

// IsSeparator mocks base method
func (m *MockTokenizer) IsSeparator(elem string) bool {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IsSeparator", elem)
	ret0, _ := ret[0].(bool)
	return ret0
}

// IsSeparator indicates an expected call of IsSeparator
func (mr *MockTokenizerMockRecorder) IsSeparator(elem interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IsSeparator", reflect.TypeOf((*MockTokenizer)(nil).IsSeparator), elem)
}

// IsOpening mocks base method
func (m *MockTokenizer) IsOpening(elem string) bool {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IsOpening", elem)
	ret0, _ := ret[0].(bool)
	return ret0
}

// IsOpening indicates an expected call of IsOpening
func (mr *MockTokenizerMockRecorder) IsOpening(elem interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IsOpening", reflect.TypeOf((*MockTokenizer)(nil).IsOpening), elem)
}

// IsClose mocks base method
func (m *MockTokenizer) IsClose(elem string) bool {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IsClose", elem)
	ret0, _ := ret[0].(bool)
	return ret0
}

// IsClose indicates an expected call of IsClose
func (mr *MockTokenizerMockRecorder) IsClose(elem interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IsClose", reflect.TypeOf((*MockTokenizer)(nil).IsClose), elem)
}

// IsContent mocks base method
func (m *MockTokenizer) IsContent(elem string) bool {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IsContent", elem)
	ret0, _ := ret[0].(bool)
	return ret0
}

// IsContent indicates an expected call of IsContent
func (mr *MockTokenizerMockRecorder) IsContent(elem interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IsContent", reflect.TypeOf((*MockTokenizer)(nil).IsContent), elem)
}

// MockStorage is a mock of Storage interface
type MockStorage struct {
	ctrl     *gomock.Controller
	recorder *MockStorageMockRecorder
}

// MockStorageMockRecorder is the mock recorder for MockStorage
type MockStorageMockRecorder struct {
	mock *MockStorage
}

// NewMockStorage creates a new mock instance
func NewMockStorage(ctrl *gomock.Controller) *MockStorage {
	mock := &MockStorage{ctrl: ctrl}
	mock.recorder = &MockStorageMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockStorage) EXPECT() *MockStorageMockRecorder {
	return m.recorder
}

// Save mocks base method
func (m *MockStorage) Save(result Result) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Save", result)
	ret0, _ := ret[0].(error)
	return ret0
}

// Save indicates an expected call of Save
func (mr *MockStorageMockRecorder) Save(result interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Save", reflect.TypeOf((*MockStorage)(nil).Save), result)
}

// GetLatest mocks base method
func (m *MockStorage) GetLatest() ([]Result, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetLatest")
	ret0, _ := ret[0].([]Result)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetLatest indicates an expected call of GetLatest
func (mr *MockStorageMockRecorder) GetLatest() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetLatest", reflect.TypeOf((*MockStorage)(nil).GetLatest))
}

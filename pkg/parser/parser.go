package parser

import (
	"fmt"
	"strings"
	"time"
)

const (
	arrayClose   = "]"
	arrayOpening = "["
)

type Tokenizer interface {
	ParseTokens(input string) ([]string, error)
	IsSeparator(elem string) bool
	IsOpening(elem string) bool
	IsClose(elem string) bool
	IsContent(elem string) bool
}

type Storage interface {
	Save(result Result) error
	GetLatest() ([]Result, error)
}

type InvalidFormat struct {
	prev    error
	message string
}

func (i InvalidFormat) Error() string {
	if i.prev != nil {
		return fmt.Sprintf("invalid format: %v", i.prev.Error())
	}

	if i.message != "" {
		return i.message
	}

	return "invalid format"
}

type Parser struct {
	tokenizer Tokenizer
	storage   Storage
}

func NewParser(tokenizer Tokenizer, storage Storage) *Parser {
	return &Parser{tokenizer: tokenizer, storage: storage}
}

type Result struct {
	Input  string    `json:"input"`
	Output string    `json:"output"`
	Depth  uint      `json:"depth"`
	Date   time.Time `json:"date"`
}

func (p Parser) Parse(input string, date time.Time) (Result, error) {
	tokens, err := p.tokenizer.ParseTokens(input)
	if err != nil {
		return Result{}, InvalidFormat{prev: err}
	}

	res, err := p.getResult(tokens)
	if err != nil {
		return Result{}, InvalidFormat{prev: err}
	}

	res.Date = date
	res.Input = input

	err = p.storage.Save(res)

	if err != nil {
		return Result{}, fmt.Errorf("error saving results: %w", err)
	}

	return res, nil
}

func (p Parser) getResult(tokens []string) (Result, error) {
	elements := []string{}

	result := arrayOpening

	prev := ""
	depth := -1
	arraysCount := 0

	for pos, token := range tokens {
		if !p.isValidOpening(pos, token) {
			return Result{}, InvalidFormat{
				message: "invalid array opening",
			}
		}

		if pos > 0 && !p.isExpected(token, prev) {
			return Result{}, InvalidFormat{
				message: fmt.Sprintf("unexpected token in position %d", pos),
			}
		}

		if p.tokenizer.IsContent(token) {
			elements = append(elements, token)
			prev = token

			continue
		}

		if p.tokenizer.IsOpening(token) {
			depth++
			arraysCount++
		}

		if p.tokenizer.IsClose(token) {
			arraysCount--
		}

		prev = token
	}

	if depth < 0 || arraysCount != 0 {
		return Result{}, InvalidFormat{
			message: fmt.Sprintf("invalid array delimiters count: %d", arraysCount),
		}
	}

	result += strings.Join(elements, ",")

	result += arrayClose

	return Result{
		Output: result,
		Depth:  uint(depth),
	}, nil
}

func (p Parser) isValidOpening(pos int, token string) bool {
	if pos > 0 {
		return true
	}

	return p.tokenizer.IsOpening(token)
}

func (p Parser) isExpected(token, prev string) bool {
	if p.tokenizer.IsSeparator(prev) {
		return p.tokenizer.IsOpening(token) || p.tokenizer.IsContent(token)
	}

	if p.tokenizer.IsClose(prev) {
		return p.tokenizer.IsSeparator(token) || p.tokenizer.IsClose(token)
	}

	if p.tokenizer.IsOpening(prev) {
		return p.tokenizer.IsOpening(token) || p.tokenizer.IsContent(token)
	}

	if p.tokenizer.IsContent(prev) {
		return p.tokenizer.IsSeparator(token) || p.tokenizer.IsClose(token)
	}

	return false
}

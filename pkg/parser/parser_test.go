package parser

import (
	"array-parser/pkg/parser/tokenizers"
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"

	"github.com/stretchr/testify/require"
)

func TestParser_ParseErrors(t *testing.T) {
	req := require.New(t)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	strg := NewMockStorage(ctrl)
	parser := NewParser(tokenizers.NewStringTokenizer(), strg)

	executionTime := time.Now()

	t.Run("Invalid array 2 level depth", func(t *testing.T) {
		input := "[25,23, [24]"

		_, err := parser.Parse(input, executionTime)

		req.Error(err)
		req.Equal("invalid format: invalid array delimiters count: 1", err.Error())
	})

	t.Run("Invalid array 2 level depth empty content", func(t *testing.T) {
		input := "[25,[,],23,24]"

		_, err := parser.Parse(input, executionTime)

		req.Error(err)
		req.Equal("invalid format: unexpected token in position 4", err.Error())
	})

	t.Run("Invalid not opened array", func(t *testing.T) {
		input := "25,23, [24]]"

		_, err := parser.Parse(input, executionTime)

		req.Error(err)
		req.Equal("invalid format: invalid array opening", err.Error())
	})

	t.Run("Error saving array", func(t *testing.T) {
		input := "[23,[22]]"
		expected := Result{
			Output: "[23,22]",
			Depth:  1,
			Date:   executionTime,
		}

		strg.EXPECT().Save(gomock.Any()).Do(func(res Result) {
			req.Equal(expected.Output, res.Output)
			req.Equal(expected.Depth, res.Depth)
			req.Equal(expected.Date, res.Date)
		}).Return(errors.New("test error"))

		parser := NewParser(tokenizers.NewStringTokenizer(), strg)

		_, err := parser.Parse(input, executionTime)

		req.Error(err)
		req.Equal("error saving results: test error", err.Error())
	})
}

func TestParser_Parse(t *testing.T) {
	req := require.New(t)
	executionTime := time.Now()

	tests := []struct {
		name    string
		input   string
		want    Result
		wantErr bool
	}{
		{
			name:  "One deep array with pictograms",
			input: `[24,A,B,Z,♠, ♣, ♥, ♦ ]`,
			want: Result{
				Output: "[24,A,B,Z,♠,♣,♥,♦]",
				Depth:  0,
				Date:   executionTime,
			},
			wantErr: false,
		},
		{
			name:  "3 depth array level",
			input: `[24,B, [ Z, [4, ABC def, [23] ]  ]]`,
			want: Result{
				Output: "[24,B,Z,4,ABC def,23]",
				Depth:  3,
				Date:   executionTime,
			},
			wantErr: false,
		},
		{
			name:  "6 depth array level",
			input: `[24,B, [ Z, [4, ABC def, [23, [45, [dr, pgb, [12, 34]]]], 56]]]`,
			want: Result{
				Output: "[24,B,Z,4,ABC def,23,45,dr,pgb,12,34,56]",
				Depth:  6,
				Date:   executionTime,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			strg := NewMockStorage(ctrl)
			strg.EXPECT().Save(gomock.Any()).Return(nil)

			p := NewParser(tokenizers.NewStringTokenizer(), strg)

			got, err := p.Parse(tt.input, executionTime)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			req.NotNil(got)

			req.Equal(tt.input, got.Input)
			req.Equal(tt.want.Output, got.Output)
			req.Equal(tt.want.Depth, got.Depth)
			req.Equal(tt.want.Date, got.Date)
		})
	}
}

func TestParser_GetLatest(t *testing.T) {
	req := require.New(t)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	strg := NewMockStorage(ctrl)

	expected := []Result{{
		Output: "[foo,bar]",
		Depth:  1,
	}}
	strg.EXPECT().GetLatest().Return(expected, nil)

	parser := NewParser(tokenizers.NewStringTokenizer(), strg)

	res, err := parser.GetLatest()

	req.NoError(err)

	req.Equal(expected, res)
}

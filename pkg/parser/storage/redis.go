package storage

import (
	"array-parser/pkg/parser"
	"array-parser/pkg/platform/redis"
	"context"
	"encoding/json"
	"fmt"
)

const storageKey = "array:results:list"

type ArrayStorage struct {
	client redis.Client
}

func NewArrayStorage(client redis.Client) *ArrayStorage {
	return &ArrayStorage{client: client}
}

func (a ArrayStorage) Save(result parser.Result) error {
	ctx := context.Background()

	res, err := json.Marshal(result)
	if err != nil {
		return fmt.Errorf("error parsing result: %w", err)
	}

	return a.client.LPush(ctx, storageKey, string(res)).Err()
}

func (a ArrayStorage) GetLatest() ([]parser.Result, error) {
	results := []parser.Result{}

	ctx := context.Background()
	res := a.client.LRange(ctx, storageKey, 0, 100)

	if err := res.Err(); err != nil {
		return nil, fmt.Errorf("error loading latest: %w", err)
	}

	for _, str := range res.Val() {
		var r parser.Result

		err := json.Unmarshal([]byte(str), &r)
		if err != nil {
			return nil, fmt.Errorf("unmarshal error: %w", err)
		}

		results = append(results, r)
	}

	return results, nil
}

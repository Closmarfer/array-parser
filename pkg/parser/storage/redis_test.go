package storage

import (
	"array-parser/pkg/parser"
	"array-parser/pkg/platform/redis"
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestArrayStorage_Save(t *testing.T) {
	req := require.New(t)

	cfg := redis.Config{Address: "redis:6379"}
	client := redis.NewRedisClient(&cfg)

	err := redis.FlushAll(client)
	req.NoError(err)

	strg := NewArrayStorage(client)

	date, err := time.Parse("2006-01-02", "2020-02-24")
	req.NoError(err)

	err = strg.Save(parser.Result{
		Input:  "[test,[34]]",
		Output: "[test,34]",
		Depth:  1,
		Date:   date,
	})

	req.NoError(err)

	ctx := context.Background()
	res := client.LPop(ctx, storageKey)

	req.NoError(res.Err())

	req.Equal(`{"input":"[test,[34]]","output":"[test,34]","depth":1,"date":"2020-02-24T00:00:00Z"}`, res.Val())
}

func TestArrayStorage_GetLatest(t *testing.T) {
	req := require.New(t)

	cfg := redis.Config{Address: "redis:6379"}
	client := redis.NewRedisClient(&cfg)

	err := redis.FlushAll(client)
	req.NoError(err)

	strg := NewArrayStorage(client)

	date, err := time.Parse("2006-01-02", "2020-02-24")
	req.NoError(err)

	expected := parser.Result{
		Output: "[test,25]",
		Depth:  2,
		Date:   date,
	}

	err = strg.Save(expected)

	req.NoError(err)

	latest, err := strg.GetLatest()

	req.NoError(err)

	req.NotEmpty(latest)

	req.Equal(expected.Output, latest[0].Output)
	req.Equal(expected.Depth, latest[0].Depth)
	req.Equal(expected.Date, latest[0].Date)
}

package tokenizers

import (
	"strings"
)

const (
	openElement      = "["
	closeElement     = "]"
	separatorElement = ","
)

type ErrorEmptyString struct{}

func (e ErrorEmptyString) Error() string {
	return "string to parse is empty"
}

type StringTokenizer struct{}

func NewStringTokenizer() *StringTokenizer {
	return &StringTokenizer{}
}

func (s StringTokenizer) ParseTokens(input string) ([]string, error) {
	input = strings.TrimSpace(input)

	if input == "" {
		return nil, ErrorEmptyString{}
	}

	tokens := s.parseTokens(input)

	return tokens, nil
}

func (s StringTokenizer) parseTokens(input string) []string {
	tokens := []string{}

	for len(input) > 0 {
		char := string(input[0])

		if char == openElement {
			tokens = append(tokens, char)
			input = input[1:]

			continue
		}

		if char == closeElement {
			tokens = append(tokens, char)
			input = input[1:]

			continue
		}

		if char == separatorElement {
			tokens = append(tokens, char)
			input = input[1:]

			continue
		}

		token := s.parseElement(input)

		tokenLen := len(token)

		token = strings.TrimSpace(token)

		if token == "" {
			input = input[tokenLen:]

			continue
		}

		tokens = append(tokens, token)

		input = input[tokenLen:]
	}

	return tokens
}

func (s StringTokenizer) IsContent(elem string) bool {
	return !s.isStructuralArrayElement(elem)
}

func (s StringTokenizer) IsSeparator(elem string) bool {
	return elem == separatorElement
}

func (s StringTokenizer) IsOpening(elem string) bool {
	return elem == openElement
}

func (s StringTokenizer) IsClose(elem string) bool {
	return elem == closeElement
}

func (s StringTokenizer) parseElement(input string) string {
	token := ""

	for _, el := range input {
		elem := string(el)
		if s.isStructuralArrayElement(elem) {
			return token
		}

		token += elem
	}

	return token
}

func (s StringTokenizer) isStructuralArrayElement(elem string) bool {
	return elem == openElement || elem == closeElement || elem == separatorElement
}

package tokenizers

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestStringTokenizer_ParseTokens(t *testing.T) {
	tokenizer := NewStringTokenizer()
	req := require.New(t)

	tests := []struct {
		name      string
		input     string
		output    []string
		depth     uint
		errorText string
	}{
		{
			name:      "Error empty string",
			input:     ``,
			output:    []string{},
			depth:     1,
			errorText: "string to parse is empty",
		},
		{
			name:   "Single element integer",
			input:  `[25]`,
			output: []string{"[", "25", "]"},
			depth:  1,
		},
		{
			name:   "Array two elements without spaces",
			input:  `[25,28]`,
			output: []string{"[", "25", ",", "28", "]"},
			depth:  1,
		},
		{
			name: "Array two levels depth multiple spaces and tabs",
			input: `[25,28  ,[A,B]	]`,
			output: []string{"[", "25", ",", "28", ",", "[", "A", ",", "B", "]", "]"},
			depth:  1,
		},
		{
			name:   "Array 2 levels depth with strings accents",
			input:  `[[10, 20, 30], 40, à]`,
			output: []string{"[", "[", "10", ",", "20", ",", "30", "]", ",", "40", ",", "à", "]"},
			depth:  1,
		},
		{
			name:   "Integer array 3 level depth",
			input:  `[[10, 20, [30]], 40, à]`,
			output: []string{"[", "[", "10", ",", "20", ",", "[", "30", "]", "]", ",", "40", ",", "à", "]"},
			depth:  1,
		},
		{
			name:   "Integer array 1 level depth with pictograms",
			input:  `[♠,♣,♥,♦]`,
			output: []string{"[", "♠", ",", "♣", ",", "♥", ",", "♦", "]"},
			depth:  1,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			out, err := tokenizer.ParseTokens(test.input)

			if len(test.errorText) > 0 {
				req.Error(err)
				req.Equal(test.errorText, err.Error())

				return
			}

			req.NoError(err)

			req.Equal(test.output, out)
		})
	}
}

package redis

import (
	"context"
	"sync"

	"github.com/go-redis/redis/v8"
)

// Client is our wrapper for the external interface. In this case we reuse it all.
type Client interface {
	redis.Cmdable
}

type Config struct {
	Address string
}

// NewRedisClient creates a Redis client for the Cache.
func NewRedisClient(cfg *Config) Client {
	var (
		storageOnce       sync.Once
		storageConnection *redis.Client
	)

	storageOnce.Do(func() {
		storageConnection = redis.NewClient(&redis.Options{
			Addr: cfg.Address,
		})
	})

	return storageConnection
}

// FlushAll simulates a `FLUSHALL` on the cluster. Use only in testing.
// For testing purposes, the Redis Cluster for local and testing consists of one node with one partition.
// For that reason, executing a FlushDB is enough in testing.
func FlushAll(client Client) error {
	ctx := context.Background()

	return client.FlushDB(ctx).Err()
}

package redis

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewRedisClientStorage(t *testing.T) {
	req := require.New(t)
	cfg := Config{Address: "redis:6379"}
	client := NewRedisClient(&cfg)

	ctx := context.Background()
	err := FlushAll(client)

	req.NoError(err)

	status := client.Set(ctx, "test:string", "test:value", 0)
	req.NoError(status.Err())

	res := client.Get(ctx, "test:string")
	req.NoError(res.Err())
	req.Equal("test:value", res.Val())
}

package serviceprovider

import (
	"array-parser/pkg/parser"
	"array-parser/pkg/parser/storage"
	"array-parser/pkg/parser/tokenizers"
	"array-parser/pkg/platform/redis"
)

func ProvideRedisClient(config redis.Config) redis.Client {
	return redis.NewRedisClient(&config)
}

func ProvideParser(config redis.Config) *parser.Parser {
	return parser.NewParser(tokenizers.NewStringTokenizer(), ProvideArrayStorage(ProvideRedisClient(config)))
}

func ProvideArrayStorage(client redis.Client) *storage.ArrayStorage {
	return storage.NewArrayStorage(client)
}
